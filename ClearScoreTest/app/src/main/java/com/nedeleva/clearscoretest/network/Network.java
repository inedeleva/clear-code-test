package com.nedeleva.clearscoretest.network;

import com.nedeleva.clearscoretest.callbacks.NetworkCallback;
import com.nedeleva.clearscoretest.models.MockCreditValues;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ivanedeleva on 12/22/16.
 */

public class Network {

    public static void getCreditScore(final NetworkCallback callback) {
        NetworkUtil.getClient().getMockCreditValues().enqueue(new Callback<MockCreditValues>() {
            @Override
            public void onResponse(Call<MockCreditValues> call, Response<MockCreditValues> response) {
                if (response == null) {
                    callback.onError();
                } else if (response.code() == 200) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onError(response.code());
                }
            }

            @Override
            public void onFailure(Call<MockCreditValues> call, Throwable t) {
                callback.onError();
            }
        });
    }
}
