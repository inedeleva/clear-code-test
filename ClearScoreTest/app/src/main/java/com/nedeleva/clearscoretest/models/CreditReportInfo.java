package com.nedeleva.clearscoretest.models;

/**
 * Created by ivanedeleva on 12/22/16.
 */
public class CreditReportInfo {

    private int score;
    private int scoreBand;
    private String clientRef;
    private String status;
    private int maxScoreValue;
    private int minScoreValue;
    private int monthsSinceLastDefault;
    private boolean hasEverDefaulted;
    private int monthsSinceLastDelinquent;
    private boolean hasEvenBeenDelinquent;
    private int percentageCreditUsed;
    private int percentageCreditUserDirectionFlag;
    private int changedScore;
    private int currentShortTermDebt;
    private int currentShortTermNonPromotionalDebt;
    private int currentShortTermCreditLimit;
    private int currentShortTermCreditUtilisation;
    private int changeInShortTermDebt;
    private int currentLongTermDebt;
    private int currentLongTermNonPromotionalDebt;
    private String currentLongTermCreditLimit;
    private String currentLongTermCreditUtilisation;
    private int changeInLongTermDebt;
    private int numPositiveScoreFactors;
    private int numNegativeScoreFactors;
    private int equifaxScoreBand;
    private String equifaxScoreBandDescription;
    private int daysUntilNextReport;

    public CreditReportInfo() {
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScoreBand() {
        return scoreBand;
    }

    public void setScoreBand(int scoreBand) {
        this.scoreBand = scoreBand;
    }

    public String getClientRef() {
        return clientRef;
    }

    public void setClientRef(String clientRef) {
        this.clientRef = clientRef;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMaxScoreValue() {
        return maxScoreValue;
    }

    public void setMaxScoreValue(int maxScoreValue) {
        this.maxScoreValue = maxScoreValue;
    }

    public int getMinScoreValue() {
        return minScoreValue;
    }

    public void setMinScoreValue(int minScoreValue) {
        this.minScoreValue = minScoreValue;
    }

    public int getMonthsSinceLastDefault() {
        return monthsSinceLastDefault;
    }

    public void setMonthsSinceLastDefault(int monthsSinceLastDefault) {
        this.monthsSinceLastDefault = monthsSinceLastDefault;
    }

    public boolean isHasEverDefaulted() {
        return hasEverDefaulted;
    }

    public void setHasEverDefaulted(boolean hasEverDefaulted) {
        this.hasEverDefaulted = hasEverDefaulted;
    }

    public int getMonthsSinceLastDelinquent() {
        return monthsSinceLastDelinquent;
    }

    public void setMonthsSinceLastDelinquent(int monthsSinceLastDelinquent) {
        this.monthsSinceLastDelinquent = monthsSinceLastDelinquent;
    }

    public boolean isHasEvenBeenDelinquent() {
        return hasEvenBeenDelinquent;
    }

    public void setHasEvenBeenDelinquent(boolean hasEvenBeenDelinquent) {
        this.hasEvenBeenDelinquent = hasEvenBeenDelinquent;
    }

    public int getPercentageCreditUsed() {
        return percentageCreditUsed;
    }

    public void setPercentageCreditUsed(int percentageCreditUsed) {
        this.percentageCreditUsed = percentageCreditUsed;
    }

    public int getPercentageCreditUserDirectionFlag() {
        return percentageCreditUserDirectionFlag;
    }

    public void setPercentageCreditUserDirectionFlag(int percentageCreditUserDirectionFlag) {
        this.percentageCreditUserDirectionFlag = percentageCreditUserDirectionFlag;
    }

    public int getChangedScore() {
        return changedScore;
    }

    public void setChangedScore(int changedScore) {
        this.changedScore = changedScore;
    }

    public int getCurrentShortTermDebt() {
        return currentShortTermDebt;
    }

    public void setCurrentShortTermDebt(int currentShortTermDebt) {
        this.currentShortTermDebt = currentShortTermDebt;
    }

    public int getCurrentShortTermNonPromotionalDebt() {
        return currentShortTermNonPromotionalDebt;
    }

    public void setCurrentShortTermNonPromotionalDebt(int currentShortTermNonPromotionalDebt) {
        this.currentShortTermNonPromotionalDebt = currentShortTermNonPromotionalDebt;
    }

    public int getCurrentShortTermCreditLimit() {
        return currentShortTermCreditLimit;
    }

    public void setCurrentShortTermCreditLimit(int currentShortTermCreditLimit) {
        this.currentShortTermCreditLimit = currentShortTermCreditLimit;
    }

    public int getCurrentShortTermCreditUtilisation() {
        return currentShortTermCreditUtilisation;
    }

    public void setCurrentShortTermCreditUtilisation(int currentShortTermCreditUtilisation) {
        this.currentShortTermCreditUtilisation = currentShortTermCreditUtilisation;
    }

    public int getChangeInShortTermDebt() {
        return changeInShortTermDebt;
    }

    public void setChangeInShortTermDebt(int changeInShortTermDebt) {
        this.changeInShortTermDebt = changeInShortTermDebt;
    }

    public int getCurrentLongTermDebt() {
        return currentLongTermDebt;
    }

    public void setCurrentLongTermDebt(int currentLongTermDebt) {
        this.currentLongTermDebt = currentLongTermDebt;
    }

    public int getCurrentLongTermNonPromotionalDebt() {
        return currentLongTermNonPromotionalDebt;
    }

    public void setCurrentLongTermNonPromotionalDebt(int currentLongTermNonPromotionalDebt) {
        this.currentLongTermNonPromotionalDebt = currentLongTermNonPromotionalDebt;
    }

    public String getCurrentLongTermCreditLimit() {
        return currentLongTermCreditLimit;
    }

    public void setCurrentLongTermCreditLimit(String currentLongTermCreditLimit) {
        this.currentLongTermCreditLimit = currentLongTermCreditLimit;
    }

    public String getCurrentLongTermCreditUtilisation() {
        return currentLongTermCreditUtilisation;
    }

    public void setCurrentLongTermCreditUtilisation(String currentLongTermCreditUtilisation) {
        this.currentLongTermCreditUtilisation = currentLongTermCreditUtilisation;
    }

    public int getChangeInLongTermDebt() {
        return changeInLongTermDebt;
    }

    public void setChangeInLongTermDebt(int changeInLongTermDebt) {
        this.changeInLongTermDebt = changeInLongTermDebt;
    }

    public int getNumPositiveScoreFactors() {
        return numPositiveScoreFactors;
    }

    public void setNumPositiveScoreFactors(int numPositiveScoreFactors) {
        this.numPositiveScoreFactors = numPositiveScoreFactors;
    }

    public int getNumNegativeScoreFactors() {
        return numNegativeScoreFactors;
    }

    public void setNumNegativeScoreFactors(int numNegativeScoreFactors) {
        this.numNegativeScoreFactors = numNegativeScoreFactors;
    }

    public int getEquifaxScoreBand() {
        return equifaxScoreBand;
    }

    public void setEquifaxScoreBand(int equifaxScoreBand) {
        this.equifaxScoreBand = equifaxScoreBand;
    }

    public String getEquifaxScoreBandDescription() {
        return equifaxScoreBandDescription;
    }

    public void setEquifaxScoreBandDescription(String equifaxScoreBandDescription) {
        this.equifaxScoreBandDescription = equifaxScoreBandDescription;
    }

    public int getDaysUntilNextReport() {
        return daysUntilNextReport;
    }

    public void setDaysUntilNextReport(int daysUntilNextReport) {
        this.daysUntilNextReport = daysUntilNextReport;
    }
}
