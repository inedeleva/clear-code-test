package com.nedeleva.clearscoretest.callbacks;

import com.nedeleva.clearscoretest.models.MockCreditValues;

/**
 * Created by ivanedeleva on 12/22/16.
 */

public interface NetworkCallback {

    void onSuccess(MockCreditValues result);

    void onError(int code);

    void onError();
}
