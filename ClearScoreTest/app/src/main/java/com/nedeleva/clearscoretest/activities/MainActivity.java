package com.nedeleva.clearscoretest.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nedeleva.clearscoretest.callbacks.NetworkCallback;
import com.nedeleva.clearscoretest.models.MockCreditValues;
import com.nedeleva.clearscoretest.network.Network;
import com.nedeleva.clearscoretest.utils.Generic;

import nedeleva.com.clearscore.R;

public class MainActivity extends AppCompatActivity implements NetworkCallback {

    private ProgressBar creditBar;
    private TextView userScore, maxScore;
    private ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        creditBar = (ProgressBar) findViewById(R.id.credit_progress);
        userScore = (TextView) findViewById(R.id.user_score);
        maxScore = (TextView) findViewById(R.id.max_score);

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setTitle(R.string.loading_title);
        loadingDialog.setMessage(getString(R.string.loading_message));
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadingDialog.setIndeterminate(true);

        if (Generic.isConnectedToNetwork(this)) {
            loadingDialog.show();
            Network.getCreditScore(this);
        } else {
            Generic.showOfflineErrorDialog(this);
        }

    }

    @Override
    public void onSuccess(MockCreditValues result) {
        loadingDialog.dismiss();
        creditBar.setMax(result.getCreditReportInfo().getMaxScoreValue());
        creditBar.setProgress(result.getCreditReportInfo().getScore());
        userScore.setText(String.valueOf(result.getCreditReportInfo().getScore()));
        maxScore.setText(String.format(getString(R.string.out_of), result.getCreditReportInfo().getMaxScoreValue()));
    }

    @Override
    public void onError(int code) {
        loadingDialog.dismiss();
        Generic.showResultNotOkErrorDialog(this, code);
    }

    @Override
    public void onError() {
        loadingDialog.dismiss();
        Generic.showServerErrorDialog(this);
    }
}
