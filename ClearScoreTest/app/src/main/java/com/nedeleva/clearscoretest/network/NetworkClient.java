package com.nedeleva.clearscoretest.network;

import com.nedeleva.clearscoretest.models.MockCreditValues;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ivanedeleva on 12/22/16.
 */

public interface NetworkClient {

    @GET("/prod/mockcredit/values")
    Call<MockCreditValues> getMockCreditValues();
}
