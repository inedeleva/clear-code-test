package com.nedeleva.clearscoretest.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ivanedeleva on 12/22/16.
 */

public class NetworkUtil {

    private static final String BASE_URL = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit retrofit;
    private static NetworkClient client;

    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            httpClient.connectTimeout(10, TimeUnit.SECONDS);
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }

    public static NetworkClient getClient() {
        if (client == null) {
            client = getRetrofit().create(NetworkClient.class);
        }
        return client;
    }
}
