package com.nedeleva.clearscoretest.models;

/**
 * Created by ivanedeleva on 12/22/16.
 */

public class MockCreditValues {

    private String accountIDVStatus;
    private CreditReportInfo creditReportInfo;
    private String dashboardStatus;
    private String personaType;
    private CoachingSummary coachingSummary;
    private String augmentedCreditScore;

    public MockCreditValues() {
    }

    public String getAccountIDVStatus() {
        return accountIDVStatus;
    }

    public void setAccountIDVStatus(String accountIDVStatus) {
        this.accountIDVStatus = accountIDVStatus;
    }

    public CreditReportInfo getCreditReportInfo() {
        return creditReportInfo;
    }

    public void setCreditReportInfo(CreditReportInfo creditReportInfo) {
        this.creditReportInfo = creditReportInfo;
    }

    public String getDashboardStatus() {
        return dashboardStatus;
    }

    public void setDashboardStatus(String dashboardStatus) {
        this.dashboardStatus = dashboardStatus;
    }

    public String getPersonaType() {
        return personaType;
    }

    public void setPersonaType(String personaType) {
        this.personaType = personaType;
    }

    public CoachingSummary getCoachingSummary() {
        return coachingSummary;
    }

    public void setCoachingSummary(CoachingSummary coachingSummary) {
        this.coachingSummary = coachingSummary;
    }

    public String getAugmentedCreditScore() {
        return augmentedCreditScore;
    }

    public void setAugmentedCreditScore(String augmentedCreditScore) {
        this.augmentedCreditScore = augmentedCreditScore;
    }
}
