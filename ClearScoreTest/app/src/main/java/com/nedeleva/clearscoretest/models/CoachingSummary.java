package com.nedeleva.clearscoretest.models;

/**
 * Created by ivanedeleva on 12/22/16.
 */
public class CoachingSummary {

    private boolean activeTodo;
    private boolean activeChat;
    private int numberOfTodoItems;
    private int numberOfCompletedTodoItems;
    private boolean selected;

    public CoachingSummary() {
    }

    public boolean isActiveTodo() {
        return activeTodo;
    }

    public void setActiveTodo(boolean activeTodo) {
        this.activeTodo = activeTodo;
    }

    public boolean isActiveChat() {
        return activeChat;
    }

    public void setActiveChat(boolean activeChat) {
        this.activeChat = activeChat;
    }

    public int getNumberOfTodoItems() {
        return numberOfTodoItems;
    }

    public void setNumberOfTodoItems(int numberOfTodoItems) {
        this.numberOfTodoItems = numberOfTodoItems;
    }

    public int getNumberOfCompletedTodoItems() {
        return numberOfCompletedTodoItems;
    }

    public void setNumberOfCompletedTodoItems(int numberOfCompletedTodoItems) {
        this.numberOfCompletedTodoItems = numberOfCompletedTodoItems;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
